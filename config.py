"""
Fichier de configuration
"""
# Path du projet
pathProject = "/home/pi/Documents/Projet/automatisationvolet/"

# Path des logs
pathLog = pathProject + "logs/"

# Path de la BDD
pathBDD = pathProject + "bdd.json"

# Adresse IP
adresseIP = "192.168.1.42"

# Etats possibles des volets
state = ["ouvert", "ferme", "my"]