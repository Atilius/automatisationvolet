#!/usr/bin/python3
# -*- coding: utf-8 -*-

import RPi.GPIO as gpio
import time
from ThreadHoraire import ThreadHoraire
import bdd
from fonctionTime import deltatime, getTodayAsTime, getDayOfTheWeek
import datetime
from Information import Information

def ouverture():
    """
    Vérification de l'heure pour l'ouverture des volets.
    S'il est l'heure, on ouvre les volets
    """

    # Pour chaque pièce présente dans la bdd
    for piece, value in bdd.getJSON().items() :

        # Heure de levé du soleil bornée
        # leve = threadHeure.getHoraire(piece, "ouverture")
        leve = value["ouverture"]["heure"]
        leve = leve.split(':')
        leve = datetime.time(int(leve[0]),int(leve[1]),int(leve[2]))

        # Récupération de l'heure de couchée
        # soir = value["fermeture"][getDayOfTheWeek()]["borne1"]
        # soir = soir.split(':')
        # soir = datetime.time(int(soir[0]),int(soir[1]))
        # soir = deltatime(soir, minutes=-5)

        actuelle = getTodayAsTime()

        # S'il est l'heure et que le volet n'est pas ouvert
        # print("ouverture", leve, actuelle, soir)
        if leve <= actuelle <= deltatime(leve, minutes=1) :
            if value["etat"] != "ouvert":
                # Modifie la bdd
                bdd.setState(piece, "ouvert")
                # Ajout l'ouverture dans les logs
                Information("Ouverture", piece).addLog()
                # Appui sur la télécommande
                appui(value["pinUp"])

def fermeture():
    """
    Vérification de l'heure pour la fermeture des volets.
    S'il est l'heure, on ferme les volets
    """

    # Pour chaque pièce présente dans la bdd
    for piece, value in bdd.getJSON().items() :

        # Heure de couché du soleil bornée
        # couche = threadHeure.getHoraire(piece, "fermeture")
        couche = value["fermeture"]["heure"]
        couche = couche.split(':')
        couche = datetime.time(int(couche[0]), int(couche[1]), int(couche[2]))

        # Récupération de l'heure de levée
        # matin = value["ouverture"][getDayOfTheWeek(offset=1)]["borne1"]
        # matin = matin.split(':')
        # matin = datetime.time(int(matin[0]), int(matin[1]))
        # matin = deltatime(matin, minutes=-5)

        actuelle = getTodayAsTime()

        # S'il est l'heure et que le volet n'est pas fermé
        # print("fermeture", couche, actuelle, matin,)
        # if couche <= actuelle or actuelle <= matin:
        if couche <= actuelle <= deltatime(couche, minutes=1):
            if value["etat"] != "ferme":
                # Modifie la bdd
                bdd.setState(piece, "ferme")
                # Ajout la fermeture dans les logs
                Information("Fermeture", piece).addLog()
                # Appui sur la télécommande
                appui(value["pinDown"])

def initThread():
    Information("Attente des threads...").print(Information.WARNING)
    while threadHeure.available != True :
        time.sleep(0.1)
    Information("Threads OK").print(Information.OKGREEN)

def initGPIO():
    Information("Initialisation de pins GPIO...").print(Information.WARNING)
    # Initialisation des pins
    gpio.setmode(gpio.BCM)
    gpio.setwarnings(False)

    for piece, value in bdd.getJSON().items() :
        if type(value["pinUp"]) == int :
            gpio.setup(value["pinUp"], gpio.OUT, initial=gpio.HIGH)
        if type(value["pinMy"]) == int :
            gpio.setup(value["pinMy"], gpio.OUT, initial=gpio.HIGH)
        if type(value["pinDown"]) == int :
            gpio.setup(value["pinDown"], gpio.OUT, initial=gpio.HIGH)
    Information("Pin GPIO OK").print(Information.OKGREEN)

def appui(pin, tempsAppui=0.5):
    # Agit sur la télécommande
    gpio.output(pin, gpio.LOW)
    time.sleep(tempsAppui)
    gpio.output(pin, gpio.HIGH)


#----------------MAIN----------------#

if __name__ == "__main__":

    # Temps entre deux boucles de vérification.
    delay = 0.5

    # Réinitilise les logs
    Information("Début du programme").addLog()

    # Déclaration des threads
    threadHeure = ThreadHoraire()
    threadHeure.start()

    # S'assure de la mise en place des threads
    initThread()

    # Mise en place des GPIO
    initGPIO()


    # ************************ M A I N ************************#

    # Boucle principale de vérification
    while True:

        # Vérifie l'ouverture des volets
        ouverture()

        # Vérifie la fermeture des volets
        fermeture()

        # Délai entre deux vérifications
        time.sleep(delay)
