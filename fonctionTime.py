from datetime import datetime, time
from math import floor

def getTodayAsTime() -> time:
    """
    Retourne l'heure actuelle sous la forme time().
    """
    actuelle = datetime.today()
    return time(actuelle.hour,actuelle.minute, actuelle.second)

def getDayOfTheWeek(offset: int = 0) -> str :
    """
    Retourne le jour actuelle en string.
    """
    value = datetime.today().weekday() + offset
    value %= 7
    if value == 0 :
        return "lundi"
    elif value == 1 :
        return "mardi"
    elif value == 2 :
        return "mercredi"
    elif value == 3 :
        return "jeudi"
    elif value == 4 :
        return "vendredi"
    elif value == 5 :
        return "samedi"
    elif value == 6 :
        return "dimanche"

def deltatime(value: time, heures: int = 0, minutes: int = 0, secondes: int = 0) -> time:
    """
    Retourne la nouvelle heure après l'avoir décalé. Les valeurs peuvent être positives et négatives.
    """
    # conversion si les valeurs sont supérieurs à 60 secondes ou minutes
    # minutes += int(secondes / 60)
    # secondes = secondes%60

    # heures += int(minutes / 60)
    # minutes = minutes%60


    # Calcul des nouvelles secondes
    nouvelleSeconde = value.second + secondes

    if nouvelleSeconde >= 60:

        nouvelleSeconde -= 60
        minutes += 1

    elif nouvelleSeconde < 0:

        nouvelleSeconde += 60
        minutes -= 1

    # Calcul des nouvelles minutes

    nouvelleMinute = value.minute + minutes

    if nouvelleMinute >= 60:

        nouvelleMinute -= 60
        heures += 1

    elif nouvelleMinute < 0:

        nouvelleMinute += 60
        heures -= 1

    # Calcul des nouvelles heures
    nouvelleHeure = value.hour + heures

    if nouvelleHeure >= 24:
        nouvelleHeure -= 24

    # return time(nouvelleHeure, nouvelleMinute, nouvelleSeconde)
    return time(nouvelleHeure, nouvelleMinute)

def borner(value: time, minimum: time, maximum: time) -> time:
    """
    Retourne l'heure bornée.
    """
        
    if value < minimum :
        return minimum
        
    if value > maximum :
        return maximum

    return value

def secondToTime(seconds: int) -> str:
    """
    Retourne le temps des secondes passées en paramètre.
    """
    seconds = floor(seconds)
    seconds = seconds % (24 * 3600) 
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60

    return time(hour,minutes,seconds).isoformat()

def calculHeure(mode: str) -> time:
    """
    Retourne l'heure de levé ou couché du soleil
    avec la borne et la correction.
    """
