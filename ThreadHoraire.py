from threading import Thread
from HoraireAPI import HoraireAPI
from time import sleep, time
import datetime
import bdd
from Information import Information
from fonctionTime import secondToTime, getDayOfTheWeek, deltatime, borner

class ThreadHoraire(Thread):
    """
        Classe Thread qui permet de faire un appel à l'API pendant le temps
        voulu.
        Cette classe met à jour la bdd.
    """

    def __init__(self):
        """
            Initialisation du thread.
            Le temps de rafraichissement est le temps entre deux appels API pour mettre à jour la BDD.
        """
        Thread.__init__(self)
        self.daemon = True
        self.setName("Thread Horaire automatisationVolet")

        self.rawHeureLeve : datetime.time()
        self.rawHeureCouche : datetime.time()

        self.nombreEchec = 0
        self.setFrequence(heure=4)
        self.setFrequenceHorsConnexion(minute=30)
        self.available = False
        self.debug = False


    def setFrequence(self, heure : int = 0, minute : int = 0, seconde : int = 0) -> None :
        """
        Définit le temps de raffraichissement entre deux appels
        """
        resultat = heure * 3600
        resultat += minute * 60
        resultat += seconde

        self.__frequence = resultat

    def setFrequenceHorsConnexion(self, heure : int = 0, minute : int = 0, seconde : int = 0) -> None :
        """
        Définit le temps de raffraichissement entre deux appels lors d'une perte de connexion
        """
        resultat = heure * 3600
        resultat += minute * 60
        resultat += seconde

        self.__frequenceHorsConnexion = resultat

    def formatageBorne(self, texte : str) -> datetime.time:
        """
        Retourne la borne formatée.
        """
        texte = texte.split(':')
        return datetime.time(int(texte[0]), int(texte[1]))

    def getHoraireBornee(self, piece: str, mode: str, jour: str = getDayOfTheWeek(), data = bdd.getJSON()) -> datetime.time:
        """
        Retourne l'heure de levée ou couchée du soleil bornée et corrigée.
        """
        try:
            # Récupération des bornes de la pièce pour le jour d'aujourd'hui
            borne1 = self.formatageBorne(data[piece][mode][jour]["borne1"])

            borne2 = self.formatageBorne(data[piece][mode][jour]["borne2"])

            # Récupération de l'heure de levé du soleil
            if mode == "ouverture":
                heure = self.rawHeureLeve
            else:
                heure = self.rawHeureCouche

            # Ajout de la correction entre le temps donné du levé/couché du soleil et l'heure réelle
            correction = data[piece][mode]["correction"].split(':')
            heure = deltatime(heure, heures=int(correction[0]), minutes=int(correction[1]))

            # Borner l'heure donnée
            return borner(heure, borne1, borne2)

        except:

            Information("Pas de borne").print(Information.UNDERLINE)

            if mode == "ouverture":
                heure = self.rawHeureLeve
            else:
                heure = self.rawHeureCouche

            # Ajout de la correction entre le temps donné du levé/couché du soleil et l'heure réelle
            correction = data[piece][mode]["correction"].split(':')
            heure = deltatime(heure, heures=int(correction[0]), minutes=int(correction[1]))

            # Borner l'heure donnée
            return heure

    def updateHoraire(self):
        """
        Met à jour le fichier JSON pour l'heure d'ouverture du jour
        """
        data = bdd.getJSON()
        modeExistant = ["ouverture", "fermeture"]
        # jours = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]

        for piece, value in data.items():
            for mode in modeExistant :
                data[piece][mode]["heure"] = self.getHoraireBornee(piece, mode, data=data).isoformat()

        if self.debug == False:
            bdd.setJSON(data)
        else:
            print(data)
        pass

    def checkAvailability(self) -> bool:
        """
        S'assure de la disponibilité lors de la première exécution.
        """
        while self.available == False:
            try:
                self.rawHeureCouche
                self.rawHeureLeve
            except AttributeError:
                self.available = False
            else:
                self.available = True
        return True


    def run(self):
        """
        Fonction principale.
        """
        # Instanciation de L'API
        web = HoraireAPI()

        # Variable pour l'initialisation du timer
        init = 0

        delay = self.__frequence

        while True :
            
            # On essaye d'appeller l'API
            try:

                # On appel l'API
                self.rawHeureLeve = web.getHeure('sunrise')
                self.rawHeureCouche = web.getHeure('sunset')

            # Si on n'y arrive pas
            except:

                self.nombreEchec += 1

                # Au bout de 3 warnings, on affiche en rouge
                if self.nombreEchec <= 3:
                    color = Information.WARNING
                else:
                    color = Information.FAIL
                
                # On initialise le timer pour savoir le temps de déconnexion
                if self.nombreEchec == 1:
                    init = time()
                    delay = self.__frequenceHorsConnexion

                    
                texte = "Erreur de connexion - échec : {} - déconnecté depuis {}".format(self.nombreEchec, secondToTime(time()-init))
                Information(texte).print(color)
                # print(color+"Erreur de connexion, echec :", self.nombreEchec, Color.ENDC)

                continue
            
            # S'assure de la disponibilité lors de la première exécution
            self.checkAvailability()
            
            # Ecriture dans le JSON
            self.updateHoraire()
            Information("Mise à jour Horaire").addLog()

            # Si la connexion est rétablie
            if self.nombreEchec != 0:

                texte = "Connecté - temps de déconnexion : {}".format(secondToTime(time()-init))
                Information(texte).print(Information.OKGREEN)
                # Color.printColor("Connecté", Color.OKGREEN)
                self.nombreEchec = 0
                init = 0
                delay = self.__frequence


            #Trois fois par jour
            sleep(delay)

        return super().run()
