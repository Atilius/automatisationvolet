from datetime import datetime
import config

class Information():
    
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    def __init__(self, *text):        
        self.__text = text
        self.__pathLog = config.pathLog + self.fileName()

    def setPathLog(self, path: str):
        self.__pathLog = path + self.fileName()

    def fileName(self) -> str:
        name = "log_" + datetime.now().strftime("%d%m")
        return name
    
    def setText(self, *text):
        self.__text = text

    def __prefix(self) -> str:
        """
        docstring
        """
        return datetime.now().strftime("%d-%m %H:%M:%S")

    def __miseEnForme(self):
        """
        docstring
        """
        liste = list()
        for elt in self.__text:
            if type(elt) != str():
                elt = str(elt)
            liste.append(elt)
        result = self.__prefix() + ' ' + ' '.join(liste)
        return result

    # def initialisationFile(self):
    #     with open(self.__pathLog, 'w') as file:
    #         file.write(self.__miseEnForme())

    def print(self, color):
        """
        Add the text to the log file and show the text in terminal. 
        """
        self.addLog()
        print(color, self.__miseEnForme(), self.ENDC)

    def addLog(self):
        """
        Add the text to the log file.
        """
        with open(self.__pathLog, 'a') as file:
            file.write(self.__miseEnForme()+"\n")