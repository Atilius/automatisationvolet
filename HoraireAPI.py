from urllib3 import PoolManager
from certifi import where
import datetime
from time import localtime
from json import loads

class HoraireAPI():
    """
    Récupère l'heure de levé et couché du soleil à partir d'une API. 
    """

    def __init__(self, latitude : str = '50.453925', longitude : str = '2.684734'):
        """
        Instanciation de la classe
        """
        self.latitude = latitude
        self.longitude = longitude

    def getData(self, date: str = 'today') -> dict :
        """
        Retourne le JSON de l'ensemble des données depuis l'API.

        date : YYYY-MM-DD
        """
        http = PoolManager(cert_reqs='CERT_REQUIRED', ca_certs=where())
        url = 'https://api.sunrise-sunset.org/json?lat=' + self.latitude + '&lng=' + self.longitude + '&date=' + date
        html_doc = http.request('GET', url).data
        jsonFile = loads(html_doc)

        return jsonFile


    def getHeure(self, mode : str, date : str = 'today') -> datetime.time :
        """
        Récupère l'heure de levé et couché du soleil
        
        mode : [sunrise/sunset]
        date : YYYY-MM-DD
        """

        jsonFile=self.getData(date=date)

        if jsonFile["status"] != "OK":
            raise ConnectionError("Erreur lors de la récupération")

        texte = jsonFile["results"][mode]

        pm = 0
        if texte.find("PM") != -1 :
              pm = 12

        texte = texte[0:len(texte)-3]

        texte = texte.split(':')
        heure = int(texte[0]) + pm
        minute = int(texte[1])
        seconde = int(texte[2])

        # Test pour la gestion automatique des heures d'hiver ou d'été
        heure = heure + 1 - localtime().tm_isdst
        # Heure D'Hiver
        # heure += 1

        return datetime.time(heure, minute, seconde)

if __name__ == "__main__":
    
    web = HoraireAPI()
    print("sunrise : " + web.getHeure('sunrise').isoformat())
    print("sunset : " + web.getHeure('sunset').isoformat())