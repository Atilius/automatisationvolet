"""
Ce module permet de gerer la base de donnee JSON.
"""

import json
import config

def getJSON() -> dict :
    """
    Retourne la base de donne JSON.
    """
    with open(config.pathBDD, "r") as file :
        data = json.load(file)
    return data

def setJSON(data: dict) -> None :
    """
    Remplace l'ancienne base de donnee par celle passee en paramètre.
    """
    with open(config.pathBDD, "w") as file :
        json.dump(data, file)


def setState(room: str, state: str) -> None:
    """
    Modifie l'etat du volet dans le JSON.
    
    room: [salon/billard/salle a manger]
    state: [ouvert/ferme/my]
    """
    data = getJSON()
    room = room.lower()
    state = state.lower()
    # On s'assure d'ecrire un etat qui sera lisible plus tard par le programme
    assert state in config.state, "L'etat donne ne correspond pas aux attentes : {}".format(state)
    data[room]["etat"] = state
    setJSON(data)
