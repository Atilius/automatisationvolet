# Automatisation des Volets

Programme Python pour la gestion de volets automatiques dans la maison. :snake:  
Python ouvre et ferme le volet en fonction du levé et couché du soleil. :sunny: :waxing_crescent_moon:  
Un serveur Flask vous permet de créer une page web pour piloter vos volets comme vous le voulez ! :house_with_garden:  

## Gestion Automatique :computer:

Le programme est à configurer en fonction de votre emplacement sur la planète. :globe_with_meridians:  
Les broches seront à paramétrer pour piloter les télécommandes des volets ! :electric_plug:  

## Serveur Web :atom:

Si vos télécommandes deviennent inactives ou vous voulez jouer avec les volets depuis votre navigateur web sur téléphone, ordinateur ou tablette. :iphone: :computer:  
Le serveur sera juste à configurer en fonction de votre adresse IP :smile: