#!/usr/bin/python3
# coding: utf-8

from fbapp import app
from sys import path
path.insert(0, '/home/pi/Documents/Projet/automatisationvolet')
import config

if __name__ == "__main__":
    #app.run(debug=True)
    app.run(debug=False, host=config.adresseIP)
