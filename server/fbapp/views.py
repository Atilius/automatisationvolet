#! C:\Users\alexa\AppData\Local\Programs\Python\Python38-32\python
# coding: utf-8

# import configOS
from sys import path
path.insert(0, '/home/pi/Documents/Projet/automatisationvolet')

import config
import bdd
from Information import Information

from flask import Flask, render_template, url_for, redirect, request
from datetime import datetime
import time
import RPi.GPIO as gpio

app = Flask(__name__)

#################################################################
def initGPIO() -> None:
    gpio.setmode(gpio.BCM)
    gpio.setwarnings(False)

    for piece, value in bdd.getJSON().items() :
        if type(value["pinUp"]) == int :
            gpio.setup(value["pinUp"], gpio.OUT, initial=gpio.HIGH)
        if type(value["pinMy"]) == int :
            gpio.setup(value["pinMy"], gpio.OUT, initial=gpio.HIGH)
        if type(value["pinDown"]) == int :
            gpio.setup(value["pinDown"], gpio.OUT, initial=gpio.HIGH)

def appui(pin, tempsAppui=0.5):
    # Agit sur la télécommande
    gpio.output(pin, gpio.LOW)
    time.sleep(tempsAppui)
    gpio.output(pin, gpio.HIGH)


initGPIO()

#################################################################

@app.route('/')
def index():
    data = bdd.getJSON()
    date = datetime.now()
    h = date.hour
    m = date.minute
    s = date.second
    return render_template('index.html', ip=config.adresseIP, heure = h, minute = m, seconde = s,
    openSalon = data["salon"]["ouverture"]["heure"],
    closeSalon = data["salon"]["fermeture"]["heure"],
    openBillard = data["billard"]["ouverture"]["heure"],
    closeBillard = data["billard"]["fermeture"]["heure"],
    openSAM = data["salle a manger"]["ouverture"]["heure"],
    closeSAM = data["salle a manger"]["fermeture"]["heure"]
    )


# @app.route('/formulaire')
# def formulaire():
#     return render_template('formulaire.html', ip=ConfigOS.ip)

@app.route('/about', methods = ['GET'])
def about():
    return render_template('about.html', ip=config.adresseIP)

# @app.route('/resultat', methods = ['POST'])
# def resultat():
#     result = request.form
#     n = result['nom']
#     p = result['prenom']
#     return render_template("resultat.html", nom=n, prenom=p)


# ROUTE SALON

@app.route('/upSalon', methods = ['GET'])
def upSalon():
    json = bdd.getJSON()
    appui(json["salon"]["pinUp"])
    Information(request.remote_addr, "- Ouverture Salon").addLog()
    # bdd.setState("salon", "ferme")
    return redirect(url_for('index'))
    
@app.route('/mySalon', methods = ['GET'])
def mySalon():
    appui(bdd.getJSON()["salon"]["pinMy"])
    Information(request.remote_addr, "- My Salon").addLog()
    # bdd.setState("salon", "my")
    return redirect(url_for('index'))

@app.route('/downSalon', methods = ['GET'])
def downSalon():
    appui(bdd.getJSON()["salon"]["pinDown"])
    Information(request.remote_addr, "- Fermeture Salon").addLog()
    # bdd.setState("salon", "ferme")
    return redirect(url_for('index'))


# ROUTE BILLARD

@app.route('/upBillard', methods = ['GET'])
def upBillard():
    appui(bdd.getJSON()["billard"]["pinUp"])
    Information(request.remote_addr, "- Ouverture Billard").addLog()
    # bdd.setState("billard", "ouvert")
    return redirect(url_for('index'))
    
@app.route('/myBillard', methods = ['GET'])
def myBillard():
    appui(bdd.getJSON()["billard"]["pinMy"])
    Information(request.remote_addr, "- My Billard").addLog()
    # bdd.setState("billard", "my")
    return redirect(url_for('index'))

@app.route('/downBillard', methods = ['GET'])
def downBillard():
    appui(bdd.getJSON()["billard"]["pinDown"])
    Information(request.remote_addr, "- Fermeture Billard").addLog()
    # bdd.setState("billard", "ferme")
    return redirect(url_for('index'))


# ROUTE SALLE À MANGER
    
@app.route('/mySAM', methods = ['GET'])
def mySAM():
    appui(bdd.getJSON()["salle a manger"]["pinMy"])
    Information(request.remote_addr, "- My Salle a manger").addLog()
    # bdd.setState("salle a manger", "ouvert")
    return redirect(url_for('index'))

@app.route('/downSAM', methods = ['GET'])
def downSAM():
    appui(bdd.getJSON()["salle a manger"]["pinDown"])
    Information(request.remote_addr, "- Fermeture Salle a manger").addLog()
    # bdd.setState("salle a manger", "ferme")
    return redirect(url_for('index'))

if __name__ == "__main__":
    app.run()
